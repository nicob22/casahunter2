import React, {
  Component
} from 'react';
import Header from './components/Header';
import List from './components/List';
import axios from 'axios';
import {
  Container,
  InputGroupAddon,
  InputGroup,
  Input,
  Button,
  Pagination,
  PaginationItem,
  PaginationLink

} from 'reactstrap';

const URL = '/api/list';


class App extends Component {


  state = {
    loadingData: false,
    list: [],
    Max: 1100,
    meter: 60,
    b: 1,
    page: 1
  }
  updateList = (url) => {
    this.setState({
      loadingData: true
    }, () => {
      let newURL = URL + `?Max=${this.state.Max}&Min=${this.state.meter}&b=${this.state.b}`;

      axios.get(newURL)
        .then(res => {
          this.setState({
            list: res.data,
            loadingData: false
          });
        })
        .catch(err => console.log(err));
    });
  };

  componentDidMount() {
    this.updateList(URL);
  }
  changePage = (page) => {
    console.log(page);
    this.setState({
      page: page,
    }, () => this.updateList);
  }
  udateInput = (event) => {
    let  {name,value,type } = event.target;
    if (type === 'checkbox'){
      value = event.target.checked ? '1':'0';
    }
    console.log(value)
    console.log(name)

    this.setState({
      [name]: value
    }, () => {
      console.log(this.state);
    })
  }

  render() {
    return ( <
      div className = "App" >
      <
      Header / >
      <
      Container >

      <div className="card">
        <div className="card-body">
        <
        InputGroup >
         < /
        InputGroup >
        <form className="row">
  <div className="form-group col-lg-3 col-6">
    <label htmlFor="exampleInputEmail1">Max Price</label>
    <Input type="number" name="Max" className="form-control" onChange={this.udateInput} id="exampleInputEmail1"  placeholder="Max Price" />
  </div>
  <div className="form-group col-lg-3 col-6 ">
    <label htmlFor="exampleInputPassword1">Meter/square</label>
    <Input type="number" onChange={this.udateInput} name="meter" className="form-control" id="exampleInputPassword1" placeholder="m2" />
  </div>
  <div className="form-check col-lg-3 col-6">
    <Input type="checkbox" onChange={this.udateInput} name="b" className="form-check-input" id="exampleCheck1" />
    <label className="form-check-label" htmlFor="exampleCheck1">Balcony ?</label>
  </div><div className="col-lg-3 col-6 ">
        <Button  onClick={this.updateList} color="primary" className="float-right"> Search </Button>
        </div>
        </form>

        </div>
      </div>
      <
      List list = {
        this.state.list
      }
      />

        <Pagination aria-label="Page navigation example">


              <PaginationItem className={(this.state.page ===1) ? 'active':''}>
                <PaginationLink onClick={()=>this.changePage(1)} >
                  {this.state.page}
                </PaginationLink>
              </PaginationItem>
              <PaginationItem className={(this.state.page ===2) ? 'active':''}>
                <PaginationLink onClick={()=>this.changePage(2)} >
                  2
                </PaginationLink>
              </PaginationItem  >
              <PaginationItem className={(this.state.page ===3) ? 'active':''}>
                <PaginationLink onClick={()=>this.changePage(3)} >
                  3
                </PaginationLink>
              </PaginationItem>
              <PaginationItem className={(this.state.page ===4) ? 'active':''}>
                <PaginationLink onClick={()=>this.changePage(4)} >
                  4
                </PaginationLink>
              </PaginationItem>
              <PaginationItem className={(this.state.page ===5) ? 'active':''}>
                <PaginationLink onClick={()=>this.changePage(5)} >
                  5
                </PaginationLink>
              </PaginationItem>

            </Pagination>


      < /
      Container >

      <
      /div>
    );
  }
}

export default App;
