
import React from 'react';
import { CardColumns, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button } from 'reactstrap';

const List = (props) => {

  this.returnCard = (house) => {
    return (
      <div key={house.kpi + `${house.address}`}>
      <a href={house.href}>
      <Card >
        <CardImg top width="100%" src={house.image} alt="Card image cap" />
        <CardBody>
          <CardTitle>{house.distances}</CardTitle>
          <CardSubtitle>{house.address}</CardSubtitle>
          <CardText>{house.prezzo} 💰 {house.size} m2↔ {house.rooms} 🔲 <br/>{house.kpi}</CardText>

        </CardBody>
      </Card>
      </a>
      </div>

    )
  }
  console.log(props.list);
  return (
    <CardColumns>

  {
      props.list.map((house) => this.returnCard(house))

  }
    </CardColumns>


  );
};

export default List;
