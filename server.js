// https://scotch.io/tutorials/scraping-the-web-with-node-js

var express = require('express');
var fs = require('fs');
var request = require('request-promise');
var cheerio = require('cheerio');
var moment = require('moment');
var json = {};
var houses = [];

// app.get('/scrape', function(req, res){
// The URL we will scrape from - in our example Anchorman 2.


const ServerWG = {}

// url = 'http://www.imdb.com/title/tt1229340/';
// url = 'https://www.wg-gesucht.de/en/wohnungen-in-Berlin.8.2.0.0.html?offer_filter=1&noDeact=1&city_id=8&category=2&&rent_type=2&sMin=60&rMax=1100&radLat=52.5304441&radLng=13.408165599999961&radAdd=Lottumstra%C3%9Fe%2C+Berlin%2C+Germany&radDis=3000&rmMin=2&bal=1';
url = 'https://www.wg-gesucht.de/en/wohnungen-in-Berlin.8.2.0.0.html?offer_filter=1&noDeact=1&city_id=8&category=2&rent_type=2&sMin=60&rMax=1100&radAdd=Lottumstra%C3%9Fe+13A&radDis=20000&exc=2&rmMin=2&bal=1'
var headers = {
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
  'Accept': '*/*'
}

let options = {
  uri: url,
  headers: headers,
  transform: function(body) {
    return cheerio.load(body);
  }
};


replaceURL = (req) => {
  var Min = !req.query.meter ? '60' : req.query.meter;
  var Max = !req.query.Max ? '1100' : req.query.Max;
  var Page = !req.query.page ? '0' : req.query.page - 1;
  options.uri = `https://www.wg-gesucht.de/en/wohnungen-in-Berlin.8.2.0.${Page}.html?offer_filter=1&noDeact=1&city_id=8&category=2&rent_type=2&sMin=${Min}&rMax=${Max}&radAdd=Lottumstra%C3%9Fe+13A&radDis=30000&exc=2&rmMin=2&bal=${req.query.b}`;
  return options;
}

ServerWG.fetchdata = (req) => {
    console.log(options);
     return request(replaceURL(req))
        .then(($) => {
          console.log('Url', req.query);
          // console.log($.html());
          fs.writeFile('index.html', $.html(), function(err) {})
          var items = $('tr.offer_list_item').length;
          // $('tr.offer_list_item' ).each(function(item){
          // console.log(item)
          var data = $(this);
          // console.log('zimmer',$('td.ang_spalte_zimmer a span').text().trim())
          // console.log('rows',$('td.ang_spalte_zimmer a span').length)
          houses = [];
          for (var i = 0; i < items; i++) {
            var data = $($('td.ang_spalte_datum a span')[i]).text().trim()
            var dataPub = moment(data, 'DD.MM.YYYY')
            // console.log(dataPub, dataPub > moment().subtract(1, 'months'))
            var prezzo = $($('td.ang_spalte_miete a span b')[i]).text().trim()
            // if(dataPub < moment().subtract(14, 'days'))
            // console.log(dataPub, dataPub < moment().subtract(2, 'months'))
            if (dataPub > moment().subtract(1, 'months') && prezzo.indexOf('from') === -1) {
              json = {};
              json.rooms = parseFloat($($('td.ang_spalte_zimmer a span')[i]).text().trim(), 10)
              json.prezzo = parseInt($($('td.ang_spalte_miete a span b')[i]).text().trim().replace(' €', ''))
              json.free = $($('td.ang_spalte_freiab a span')[i]).text().trim()
              json.size = parseFloat($($('td.ang_spalte_groesse a span')[i]).text().trim().replace(' m²', ''))
              json.distances = $($('td.ang_spalte_stadt a span')[i]).text().trim()
              json.kpi = (json.prezzo / json.size);

              json.href = 'https://www.wg-gesucht.de/en/' + $($('td.ang_spalte_stadt a')[i]).attr('href')
              houses.push(json);
              // houses.push(JSON.parse(JSON.stringify(json)));
              // console.log(i, json)

            }


          }

          console.log(houses.length)
          // console.log(houses)
          fs.writeFile('output.json', JSON.stringify(houses, null, 4), function(err) {

            // console.log('File successfully written! - Check your project directory for the output.json file');

          })
          return houses;


      })
    .catch((err) => {
      console.log(err);
  });
}
module.exports = ServerWG
