const express = require('express');
const bodyParser = require('body-parser');
const ServerWG = require('./server');
const config = require('./config');
const ServerScout = require('./server0');
const path = require('path');

var port = process.env.PORT || port;
const app = express();

function compare(a, b){
  let comparison = 0;

  if (a.kpi > b.kpi) {
    comparison = 1;
  } else if (b.kpi > a.kpi) {
    comparison = -1;
  }

return comparison;
}

// if (process.env.NODE_ENV === 'production') {
app.use(express.static('client/build'));

// }

app.get('/api/list', (req, res) => {
  var scoutResult = ServerScout.fetchdata()
  .then((res1) => {
    ServerWG.fetchdata(req).then((res2)=>{
      console.log('ServerScout', res1.length);
      console.log('ServerWG', res2.length);
      var houses = res1.concat(res2);

      res.json(houses.sort(compare));

    })
  });

  app.get('*', (req, res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });

  // var houses = scoutResult.concat(wgResult)

  // console.log('scout', scout)
  //   ServerWG.fetchdata(res)
  //       .then((houses) => {
  //         console.log(houses);
  //           res.json(houses);
  //       });
});

app.use(bodyParser.json());

app.listen(config.port, () => {
        console.log(`App running on port ${config.port}`);
    });
module.exports = app
