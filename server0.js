// https://scotch.io/tutorials/scraping-the-web-with-node-js

var fs = require('fs');
var request = require('request-promise');
var cheerio = require('cheerio');
var moment = require('moment');
var json = {};
const ServerScout = {};
var houses = [];

 getUrl = (req) =>{
  var page_number = !(req) || !(req.query.page)? '1' : `${req.query.page}`
  var property_type = 'Wohnung-Miete'
  var region = 'Berlin'
  var	city = 'Berlin'
  var quarter = "Mitte-Mitte"
  var min_rooms = '2,00'
  var min_space = !(req)  || ! (req.query.meter)? '60,00' : `${req.query.meter},00`
  var min_price = '500'
  var max_price = !(req) || !(req.query.Max)? '1200' : `${req.query.Max}`
  var buy_to_let = 'false'
  var broker_fee = '-' //alterntive true
  var new_building = '-' //alterntive true
  var url =		`https://www.immobilienscout24.de/Suche/S-T/P-${page_number}/${property_type}/${region}/${city}/-/${min_rooms}-/${min_space}-/EURO-${min_price},00-${max_price},00/${buy_to_let}/-/-/-/-/-/-/-/-/-/-/-/${broker_fee}/${new_building}`
  return url;
}

    // url = 'http://www.imdb.com/title/tt1229340/';
    // url = 'https://www.immobilienscout24.de/Suche/S-T/Wohnung-Miete/Berlin/Berlin/Mitte-Mitte/2,00-/60,00-/EURO--1100,00?enteredFrom=one_step_search';
    var headers = {
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
      'Accept': '*/*'
    }

    let options = {
      uri: getUrl(),
      headers: headers,
      transform: function (body) {
        return cheerio.load(body);
      }
    };



      ServerScout.fetchdata = (req) => {
        options.uri = getUrl(req);
        return request(options)
            .then(($) => {
              // console.log($.html());
              fs.writeFile('index0.html', $.html(), function(err){
              })
              var items = $('.result-list__listing ' ).length;
              // $('tr.offer_list_item' ).each(function(item){
                // console.log(item)
                 var data = $(this);
                 // console.log('zimmer',$('td.ang_spalte_zimmer a span').text().trim())
                 // console.log('rows',$('.result-list__listing').length)
                 houses = [];
                 for(var i = 0; i < items; i++) {
                   // var data = $($('div.grid grid-flex')[i]).text().trim()
                   // var dataPub = moment(data, 'DD.MM.YYYY')
                   // console.log(dataPub, dataPub > moment().subtract(1, 'months'))
                   // var prezzo  =  $($('td.ang_spalte_miete a span b')[i]).text().trim()
                   // if(dataPub < moment().subtract(14, 'days'))
                   // console.log(dataPub, dataPub < moment().subtract(2, 'months'))
                   // if(dataPub > moment().subtract(1, 'months') && prezzo.indexOf('from') === -1) {
                     json = {};
                     var prezzo = parseInt($($($('.result-list__listing ')[i]).find('dl')[0]).find('dd').text().replace('.', '').replace(' €',''));
                     var size = parseInt($($($('.result-list__listing ')[i]).find('dl')[1]).find('dd').text().replace(' m²',''));
                     var rooms = parseInt($($($('.result-list__listing ')[i]).find('dl')[2]).find('dd .onlyLarge').text());
                     var address = $($($('.result-list__listing ')[i]).find('.result-list-entry__map-link div')).text();
                     var image = $($($('.result-list__listing ')[i]).find('.gallery-container img')).attr('data-lazy-src');
                     var href = $($($('.result-list__listing ')[i]).find('.result-list-entry__data a')).attr('href');
                     console.log('href', href);
                     if (!!href)
                      href =  href.includes('/expose/') ?
                     'https://www.immobilienscout24.de'+ href : href
                     // console.log('size', size);
                     // console.log('rooms', rooms);
                     // console.log('image', image);
                     // console.log('address', address);
                     // console.log('-------------------------------------')

                     json.href = href;
                     json.prezzo = prezzo;
                     json.kpi = (prezzo/size);
                     json.size = size
                     json.rooms = rooms
                     json.image = image
                     json.address = address
                     // json.href = 'https://www.wg-gesucht.de/en/' + $($('td.ang_spalte_miete a')[i]).attr('href')
                     houses.push(json);
                     // houses.push(JSON.parse(JSON.stringify(json)));
                     // console.log(i, json)

                   // }


                 }

                 console.log(houses.length)
                 // console.log(houses)
                 fs.writeFile('output0.json', JSON.stringify(houses, null, 4), function(err){

                     console.log('File successfully written! - Check your project directory for the output.json file');

                 })
                 return houses;

             // })
            })
            .catch((err) => {
              console.log(err);
            });
      }

module.exports = ServerScout
